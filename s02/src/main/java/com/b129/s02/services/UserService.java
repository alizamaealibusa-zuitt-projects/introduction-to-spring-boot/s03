package com.b129.s02.services;

import com.b129.s02.models.User;

public interface UserService {

    Iterable<User> getUsers();
    void createUser(User newUser);
    void updateUser(Long id, User updatedUser);
    void deleteUser(Long id);

}
