package com.b129.s02.services;

import com.b129.s02.models.Post;
import com.b129.s02.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImplementation implements PostService {

    @Autowired
    private PostRepository postRepository;

    // CRUD Functionalities
    public void createPost(Post newPost){
        postRepository.save(newPost);
    }

    public void updatePost(Long id, Post updatedPost) {
        // Find the existing record
            Post postForUpdating = postRepository.findById(id).get();
        // Do the update
            postForUpdating.setTitle(updatedPost.getTitle());
            postForUpdating.setContent(updatedPost.getContent());
        // Save the updated post
            postRepository.save(postForUpdating);
    }

    public void deletePost(Long id){
        // Post postForDeleting = postRepository.findById(id).get();
        // postRepository.delete(postForDeleting);

        postRepository.deleteById(id);
    }

    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }
}
