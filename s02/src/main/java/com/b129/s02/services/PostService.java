package com.b129.s02.services;

import com.b129.s02.models.Post;

public interface PostService {
    // CRUD functionalities
    void createPost(Post newPost);
    void updatePost(Long id, Post updatedPost);
    void deletePost(Long id);
    Iterable<Post> getPosts();
}
