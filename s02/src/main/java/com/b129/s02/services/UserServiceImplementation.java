package com.b129.s02.services;

import com.b129.s02.models.User;
import com.b129.s02.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImplementation implements UserService {

    @Autowired
    private UserRepository userRepository;

    public Iterable<User> getUsers() {
        return userRepository.findAll();
    }

    public void createUser(User newUser) {
        userRepository.save(newUser);
    }

    public void updateUser(Long id, User updatedUser) {
        User userForUpdating = userRepository.findById(id).get();

        userForUpdating.setUsername(updatedUser.getUsername());
        userForUpdating.setPassword(updatedUser.getPassword());

        userRepository.save(userForUpdating);
    }

    public void deleteUser(Long id){
        userRepository.deleteById(id);
    }
}
